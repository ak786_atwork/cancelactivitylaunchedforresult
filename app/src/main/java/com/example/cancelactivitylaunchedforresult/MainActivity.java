package com.example.cancelactivitylaunchedforresult;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Handler handler;
    private Runnable timeLimitExceedRunnable = new Runnable() {
        @Override
        public void run() {
            finishActivity(101);
            Toast.makeText(MainActivity.this, "activity closed ",Toast.LENGTH_SHORT).show();
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new Handler();
    }


    public void launchActivity(View view) {

        handler.postDelayed(timeLimitExceedRunnable,5000);
        Intent intent = new Intent(this, SecondActivity.class);
        startActivityForResult(intent,101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == 101) {
            if (Activity.RESULT_OK == resultCode) {
                Toast.makeText(this, "result ok",Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "result cancel",Toast.LENGTH_SHORT).show();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

}
